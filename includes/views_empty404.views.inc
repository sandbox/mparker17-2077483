<?php
/**
 * @file
 *
 */

/**
 * Implements hook_views_data().
 */
function views_empty404_views_data() {
  $data = array();

  $data['views']['views_handler_area_views404'] = array(
    'title' => t('404 (page not found) error'),
    'help' => t("Sends a 404 (page not found) error back to the browser.<br />Note that the view cannot be cached and will need to be executed before the 404 error is issued: in most cases using this area is a bad idea. The only case when this might make sense to use is as the no-results behaviour for views with contextual filters.<br>Note also that a 404 will not be sent if the view is being rendered by AJAX."),
    'area' => array(
      'handler' => 'views_handler_area_views404',
    ),
  );

  return $data;
}
