<?php
/**
 * @file
 * Contains views_handler_area_views404.
 */

/**
 * Views area handler to return a 404 error.
 *
 * @ingroup views_area_handlers
 */
class views_handler_area_views404 extends views_handler_area {

  /**
   * Render the 404 error.
   */
  function render($empty = FALSE) {
    // Don't give a 404 page if the view is being rendered via AJAX.
    if ($empty && !isset($this->view->ajax)) {
      drupal_not_found();
    }

    return '';
  }

}
